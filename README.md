# smartuse-luucy-test-zug

This is meant as a starting point for a Proof of Concept to integrate SmartUse into Luucy as part of a Gemeindescan.

See `screenshots/` for visual reference or [smartuse.ch/project/case-study-zug](https://smartuse.ch/project/case-study-zug)

See `datapackage.json` for a [Data Package](https://frictionlessdata.io/specs/data-package/) compliant way of referencing the individual files. E.g. to do validation, import the metadata etc. The [Smart Use API](https://smartuse.ch/api/projects/by/case-study) is based on this standard.

The list of `layers` below references resource `names` (see [Frictionless Data Resource Spec](https://frictionlessdata.io/specs/data-resource/)) as defined in the data package.

## Luucy notes

For some background on Cesium, the visualization framework used by Luucy, and additional integration questions, see:

- https://cesiumjs.org
- https://cesiumjs.org/Cesium/Build/Documentation/GeoJsonDataSource.html
- https://cesium.com/blog/2014/10/01/cesium-version-1.2-released/
- https://cesium.com/blog/2016/03/02/performance-tips-for-points/
- https://cesium.com/blog/2015/05/26/graphics-tech-in-cesium-stack/
- https://knowledge.safe.com/articles/32838/streaming-geojson-with-fme-server-2016.html
- https://docs.geoserver.org/stable/en/user/styling/sld/reference/filters.html

For a 2D dataviz demo, visit Campus Horw in Luucy.

## Layer Composition Documentation

Each example composition is followed by a screenshot.

| 1 | 30 Velominuten ab Zuger-/Ahornstrasse |
| --- | --- |
| text | Daraus ergibt sich die Notwendigkeit unter anderem Mobilität neu zu denken. Bereits mit der bestehenden Infrastruktur könnte mittels eines Fokus auf Velomobilität ein Teil der bestehenden Verkehrsbelastung gemindert werden. Hintergrund: Smart Use Lab Erreichbarkeitsisochronen |
| layers[property] | `su-mkz-zug-wohnen[WTOT]`, `su-mkz-zug-30-min-velo`, `su-mkz-zug-perimeter` |
| data/legend-types | `su-mkz-zug-wohnen[WTOT]`: `grid-bubble-range`, `su-mkz-zug-30-min-velo`: `linestring-dashed` |
| colors | n/a |

![](screenshots/01-Veloerreichbarkeit.jpg)

| 2 | Velovorteil gegenüber dem ÖV |
| --- | --- |
| text | Wo liegen jedoch die Potenziale? Wo hat das Velo als Pendlerverkehrsmittel sogar einen Vorteil gegenüber dem bestehenden ÖV-Netz? Hintergrund: Smart Use Lab Velopotenziale Erreichbarkeitsisochronen |
| layers[property] | `su-mkz-zug-30-min-velo`, `su-mkz-zug-velovorteil[DM-BA]`, `su-mkz-zug-perimeter` |
| data/legend-types | `su-mkz-zug-velovorteil[DM-BA]`: `fill-color-range`, `su-mkz-zug-30-min-velo`: `linestring-dashed` |
| colors | `su-mkz-zug-velovorteil[DM-BA]`: `linear-gradient(left, rgb(0, 0, 0) 0%, rgba(19, 220, 246, 0.5) 50%, rgba(255, 61, 200, 0.5) 100%)` |

![](screenshots/02-Velovorteil.jpg)

| 3 | Das derzeitige Einzugsgebiet Einpendler |
| --- | --- |
| text | Diesem Potenzial steht die tatsächlichen Einpendler gegenüber - gemessen anhand der Mobilfunknutzer gemessen in einer Sommer- und einer Herbstwoche in 2018. Hintergrund: Smart Use Lab Einzugsgebiete Erreichbarkeitsisochronen |
| layers[property] | `su-mkz-zug-einpendler-vzug[Count]`, `su-mkz-zug-wohnen[WTOT]`, `su-mkz-zug-30-min-velo`, `su-mkz-zug-perimeter` |
| data/legend-types | `su-mkz-zug-einpendler-vzug[Count]`: `color-range`, `su-mkz-zug-wohnen[WTOT]`: `bubble-range`, `su-mkz-zug-30-min-velo`: `dashed-line` |
| colors | `su-mkz-zug-einpendler-vzug[Count]`: `linear-gradient(left, rgba(125, 216, 227, 0.1) 0%, rgba(125, 216, 227, 0.35) 50%, rgba(125, 216, 227, 0.6) 100%)` |

![](screenshots/03-pendlereinzug.jpg)

| 4 | Velorouten mit Ziel Zuger-/Ahornstrasse |
| --- | --- |
| text | Als Grundlage für eine Gegenüberstellung von Potenzial und heutiger Nutzung, wurden diejenigen Strassensegmente selektiert, die für Velofahrten in Frage kommen. Hintergrund: Smart Use Lab OpenStreetMap Routing |
| layers[property] | `su-mkz-zug-routen`, `su-mkz-zug-wohnen[WTOT]`, `su-mkz-zug-30-min-velo`, `su-mkz-zug-perimeter` |
| data/legend-types | `su-mkz-zug-routen`: `line`, `su-mkz-zug-wohnen[WTOT]`: `bubble-range`, `su-mkz-zug-30-min-velo`: `dashed-line` |
| colors | n/a |

![](screenshots/04-velorouten.jpg)

| 5 | Einpendlergewichtet |
| --- | --- |
| text | Anhand der Einpendlerdaten können diese Strassensegmente nun mittels eines aggregierten Scores gewichtet werden. So zeigt sich, welche Segemente für eine Mobilitätswende besonders relevant wären. Hintergrund: Smart Use Lab Strassensegmentscores |
| layers[property] | `su-mkz-zug-routen[einpendler_score]`, `su-mkz-zug-30-min-velo`, `su-mkz-zug-perimeter` |
| data/legend-types | `su-mkz-zug-routen[einpendler_score]`: `line-range`, `su-mkz-zug-30-min-velo`: `dashed-line` |
| colors | n/a |

![](screenshots/05-einpendler.jpg)

| 6 | Vorteilgewichtet |
| --- | --- |
| text | Eine andere Sichtweise geht vom Idealfall des reinen Potenzials anhand des Vorteils aus. Diese Gewichtet zeigt bereits eine Asymmetrie zwischen Potenzial und gemessener Nutzung. Hintergrund: Smart Use Lab Strassensegmentscores |
| layers[property] | `su-mkz-zug-routen[bike_score]`, `su-mkz-zug-30-min-velo`, `su-mkz-zug-perimeter` |
| data/legend-types | `su-mkz-zug-routen[bike_score]`: `line-range` , `su-mkz-zug-30-min-velo`: `dashed-line` |
| colors | n/a |

![](screenshots/06-vorteilgewichtet.jpg)

| 7 | Zweite-Reihe-Effekt |
| --- | --- |
| text | Eine Besonerheit, die bereits in den selektierten Strassensegmenten ersichtlich wird, ist die Präferenz der "zweiten Reihe" und eine Meidung der Haupteinfallsachsen - wie der Zuger-/Baarerstrasse. Hintergrund: Smart Use Lab Strassensegmentscores |
| layers[property] | `su-mkz-zug-30-min-velo`, `su-mkz-zug-perimeter` |
| legend-types | `su-mkz-zug-30-min-velo`: `dashed-line` |
| colors | n/a |

![](screenshots/07-zweite-reihe.jpg)

| 8 | Benutzergenerierte Einträge auf Bikeable.ch |
| --- | --- |
| text | In der näheren Betrachtung einiger Strassensegmente können nun weitere Informationen für eine tiefergehende Analyse herangezogen werden. Zum Beispiel Nutzergenerierte Problemmeldungen der Plattform Bikeable.ch Hintergrund: Smart Use Lab Crowdsourcing |
| layers[property] | `su-mkz-zug-30-min-velo`, `su-mkz-zug-perimeter` |
| data/legend-types | `su-mkz-zug-30-min-velo`: `dashed-line` |
| colors | n/a |

![](screenshots/08-bikeable.jpg)

| 9 | Vergleich der Scores |
| --- | --- |
| text | In der Gesamtschau des Vergleichs der aggregierten Scores für Potenzial und Nutzung der Veloroutensegmente zeigt sich eine klare Asymmetrie zwischen den Routen gegen Norden und denen gegen Süden. Daraus ergibt sich ein klarer Einstiegspunkt und Anhaltspunkt für Policy-Entscheidungen. |
| layers[property] | `su-mkz-zug-routen[score_delta]`, `su-mkz-zug-30-min-velo`, `su-mkz-zug-perimeter` |
| data/legend-types | `su-mkz-zug-routen[score_delta]`: `color-range`, `su-mkz-zug-30-min-velo`: `dashed-line` |
| colors | `su-mkz-zug-routen[score_delta]`: `linear-gradient(left, rgb(26, 173, 117) 0%, rgb(145, 145, 145) 50%, rgb(230, 160, 55) 100%)` |
